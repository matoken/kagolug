# 鹿児島らぐ(Kagoshima Linux User Group)
<img src="http://kagolug.org/files/9913/9957/8865/IMG_20140411_180615-t.jpg" width=80%>

鹿児島にやってきたらLinux関係の勉強会などが見当たらない．なければ作っちゃえ!ということで2014年04月26日にソフトプラザかごしま 鹿児島大学情報技術事業化支援室にて第0回の集まりを行いました :)
そこで出来たLUG(Linux User Group)です．鹿児島で月一程度でオフ会をしています．興味のある方は是非参加してみてください．鹿児島県外の方でもオンラインで参加したり，鹿児島近辺の人に教えてあげてください．

Web : http://kagolug.org/
MailList : http://list.kagolug.org/cgi-bin/mailman/listinfo/users
Lingr : http://lingr.com/signup?letmein=kagolug




<div align=center>
LUG / BUG Map http://goo.gl/MMFQFG
<img src="map.png" width=80%>
</div>

