<!-- $theme: default -->

<!-- page_number: true -->
<!-- $size: 4:3 -->
<!-- footer: @matoken -->


# USBを保護するUSBGuard

##### KenichiroMATOHARA([@matoken](https://twitter.com/matoken/))
##### http://matoken.org 
### 鹿児島Linux勉強会2017.02.1(2017-02-18)
### 

---

## [KenichiroMATOHARA](http://matoken.org)( [@matoken](https://twitter.com/matoken) )

* 大隅在住
![](osmo.jpg)


---

* PC-UNIX/OSS, OpenStreetMap, 電子工作, 自転車……
* ロックインが嫌いでalternativeを探している

---

# Linuxで使えるものづくり系アプリ

---

# 電子工作

---

### [Arduino](https://www.arduino.cc)

* AVRベースのOHWマイコン&IDE
* ハードウェアがわからなくてもお手軽に利用できる
* プログラムはスケッチと呼ばれる

---

### gcc-avr

* Arduinoから卒業してAVRをCで利用
* 安く，容量小さく出来る

---

### Fritzing

* 回路図アプリ
* いろいろなプリセット(Arduino, Raspberry Pi...)が便利

---

## 基板作成

## 3Dプリンタ
### FreeCAD

