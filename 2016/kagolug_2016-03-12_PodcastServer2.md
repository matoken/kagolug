<!--
kagolug_2016-03-12_PodcastServer2
-->


>
Google Play ミュージックの保存容量の制限

Chrome 版 Google Play ミュージックやミュージック マネージャを使用して、個人の音楽コレクションから最大で 50,000 曲を Google Play ミュージックに追加できます（1 曲につき最大 300 MB までとなります）。音楽を追加すると、Google Play ミュージック アプリでもパソコンでも聴けるようになります。
